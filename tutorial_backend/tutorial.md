# Tutorial: Backend

This tutorial covers the EMPAIA App Description (EAD) format and the App API, that provides endpoints to retrieve and send data as specified in the EAD. The tutorial starts with a very simple app and how it can be described in EAD. During the course of this guide further features are introduced step-by-step.

All scenarios mentioned in this tutorial can be found here:

[Sample Apps Repository](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/tutorial)

?> Code examples in this tutorial are written in Python, but all HTTP service calls can be translated to other programming languages.

?> For detailed information about the EAD format and App API, see [Specs](specs/specs.md#).

## Best Practices

The examples given in the backend tutorial are simplified and only demonstrate the general usage of the App API. They do not cover every edge case a real app should be able to handle. Please keep in mind the following best practices:

* The WSI Service regions endpoint serves regions up to a limit of 5000 x 5000 px. If a region of interest (RoI) provided by the user as input is larger, the app should attempt to split up the RoI into smaller subregions for memory efficient API queries and processing.
* The WSI Service serves WSI layers as they are retrieved from the file using various image format libraries. The app should check which layer has the closest npp (nanometers per pixel) resolution, that is appropriate for the image processing model.
* If no layer has an appropriate resolution, consider scaling the image regions (preferably scaling down from a higher resolution).
* If some preconditions for a successful processing are not met (e.g. selected RoI too small), use the failure endpoint to gracefully end the processing. The failure endpoint allows the app to send a short but useful message to the user (length limit is imposed by API), that should be as descriptive as possible (e.g. "RoI size of 200 x 100 pixels provided as input is too small. Size must be at least 500 x 500 pixels.").
* The app should be tested with as many different WSIs created by different scanners as possible.
* The app should be tested with various sizes of RoIs and should cover realistic use cases, that pathologists expect to work.
* The usablity should be tested in the Workbench Client. If for example the rendering performance of the viewer is slow, consider specifying a stricter [npp_viewing](specs/annotation_resolution.md#) range for the annotations. If you are using polygons, also consider reducing the number of points per polygon.
* The app must not hardcode a specific npp resolution value for output annotations (e.g. the npp value given in the examples). Instead obtain the actual npp value of the given WSI layer provided by the App API and use it to calculate the correct annotation npp.
* Apps may use Nvidia GPUs with Cuda support for acceleration. For testing purposes and for scenarios where GPUs are (temporarily) not available, an app should provide a fallback to use only CPU processing.
* An app should be programmed to be as memory and time efficient as possible.
* An app should never become stuck in an endless loop, blocking the limited compute resources for other jobs.
