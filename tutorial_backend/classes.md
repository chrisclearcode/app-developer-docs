# Classes

In the following example, the app detects cells in  WSI and classifies the corresponding point annotations as `tumor` or `non_tumor`. These classes are defined in the scope of your app and can be assigned to EMPAIA annotations. In addition to the class scope `org.empaia.my_vendor.my_app.v1.classes`, a global class scope `org.empaia.global.v1.classes` exists and is accessible to your app. We will use the EMPAIA class `roi` to introduce a constraint, that the rectangle annotation inputs must have the class `roi` (region of interest) assigned.

In this section we will:

* Define and use classes `tumor` and `non_tumor` in class scope `org.empaia.my_vendor.my_app.v1.classes`.
* Use class `roi` from global class scope `org.empaia.global.v1.classes`.

## App Class Scope

* tumor.
* non_tumor.

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed.

## Outputs

* 1 Collection of collections of many Tumor Cell point annotations (key: `tumor_cells`). Each of the inner collections references one of the input rectangles from the `my_rectangles` collection.
* 1 Collection of collections of classification results (key: `my_cell_classes`).

## EAD

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 05",
    "name_short": "TA05",
    "namespace": "org.empaia.vendor_name.tutorial_app_05.v1",
    "description": "Human readable description",
    "classes": {
        "tumor": {
            "name": "Tumor"
        },
        "non_tumor": {
            "name": "Non tumor"
        }
    },
    "inputs": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "inputs.my_wsi",
                "classes": [
                    "org.empaia.global.v1.classes.roi"
                ]
            }
        }
    },
    "outputs": {
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "inputs.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "inputs.my_wsi"
                }
            }
        },
       "my_cell_classes": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "class",
                    "reference": "outputs.my_cells.items.items"
                }
            }
        }
    }
}
```

* App specific `classes` can be defined at the top level besides `inputs` and `outputs`.
* EMPAIA `classes` can always be accessed and must not be defined under `classes`. Here, they are used as constraint for `my_rectangles`.
* The output `my_cells` is to be persisted before the classification results `my_cell_classes` and therefore does not use the property `classes`.
* The values for classification results of `my_cell_classes` can be of any class of the app scope or global scope.

## App API Usage

```python
import os
import requests
from PIL import Image
from io import BytesIO

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    cells = {
        "item_type": "point",
        "items": [],
        "reference_id": my_rectangle[
            "id"
        ],  # point_annotations collection references my_rectangle
        "reference_type": "annotation",
    }
    cell_classes = {"item_type": "class", "items": []}
    # your computational code below
    for i in range(10):
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [250, 250],  # Always use WSI base level coordinates
            "npp_created": 499,  # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],  # (optional) recommended pixel resolution range for viewer to display annotation, if npp_created is not sufficient
        }
        cells["items"].append(cell)

        tumor_class = "org.empaia.vendor_name.tutorial_app_05.v1.classes.tumor"
        non_tumor_class = "org.empaia.vendor_name.tutorial_app_05.v1.classes.non_tumor"
        cell_class = {
            "value": tumor_class if i % 2 == 0 else non_tumor_class,  # possible values defined in EAD
            "reference_id": None,  # yet unknown
            "reference_type": "annotation",
        }
        cell_classes["items"].append(cell_class)

    return cells, cell_classes


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = {"item_type": "collection", "items": []}
my_cell_classes = {"item_type": "collection", "items": []}

for my_rectangle in my_rectangles["items"]:
    cells, cell_classes = detect_cells(my_wsi, my_rectangle)
    my_cells["items"].append(cells)
    my_cell_classes["items"].append(cell_classes)

my_cells = post_output(
    "my_cells", my_cells
)  # response includes IDs in addition to the original data

for cells, cell_classes in zip(my_cells["items"], my_cell_classes["items"]):
    for cell, cell_class in zip(cells["items"], cell_classes["items"]):
        cell_class["reference_id"] = cell["id"]  # id from POST response

post_output("my_cell_classes", my_cell_classes)

put_finalize()
```

