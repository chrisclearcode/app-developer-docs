# Additional Inputs

All Sample Apps shown so far are designed to work with the **Workbench Client 1.0** and include the *Job Wizard* to help users selecting or creating input data and starting new jobs. Due to the complexity of possible inputs the parametrization of the wizard is limited. Only a single WSI and a single ROI or ROI collection (of type rectangle, cicle or polygon) can be used as input for jobs.

Apps with app-specific App UIs are no longer restricted by these limitations for job inputs. The only remaining limitation is your own App UI (assuming the Workbench API supports the specific input type your App UI is using).

For this example we will consider an app, that processes multiple ROIs on multiple WSIs. As result, the app produces a cell count for each processed ROI and calculates an avarage cell count for each processed WSI. The processing of each ROI is parameterized by a float value, that serves as a threshold.

In this section we will:

* Demonstrate the usage of more complex inputs
* Introduce references on multiple levels in nested collections

!> The presented app will only work with the EATS CLI commands currently. Due to the more complex inputs the Workbench Client 1.0 is not able to set the necessary inputs. Therefore the job can't be started, but you can view the results of the completed jobs. The Workbench Service 2.0 will support this app as soon as the generic App UI is available in one of the next EATS releases.

## Inputs

* 1 Collection of WSIs (key: `my_wsis`)
* 1 Nested collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed on each slide
  * Each inner collection references one of the input WSIs in `my_wsis`
    * Each rectangle annotation references one of the input WSIs in `my_wsis` (the same WSI that includes the collection)
* 1 Nested collection of many float primitives (key: `my_thresholds`) specifying threshold values
  * Each float primitive references one of the rectangle annotations in `my_rectangles`

## Outputs

* 1 Nested collection of many tumor cell counts (key: `tumor_cell_counts`), where each resulting count references a rectangle annotation (from `my_rectangles`)
* 1 Collection of average tumor cell counts (key: `avg_tumor_cell_counts`), computed across all rectangle annotations on one WSI, therefore referencing a collection (the items of the input collection `my_rectangles`).

## EAD

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 09",
    "name_short": "TA09",
    "namespace": "org.empaia.vendor_name.tutorial_app_09.v1",
    "description": "Human readable description",
    "inputs": {
        "my_wsis": {
            "type": "collection",
            "items": {
                "type": "wsi"
            }
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "inputs.my_wsis.items",
                "items": {
                    "type": "rectangle",
                    "reference": "inputs.my_wsis.items"
                }
            }
        },
        "my_thresholds": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "float",
                    "reference": "inputs.my_rectangles.items.items"
                }
            }
        }
    },
    "outputs": {
        "tumor_cell_counts": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "integer",
                    "reference": "inputs.my_rectangles.items.items"
                }
            }
        },
        "avg_tumor_cell_counts": {
            "type": "collection",
            "items": {
                "type": "float",
                "reference": "inputs.my_rectangles.items"
            }
        }
    }
}
```

* Parameters of `"type": "collection"` must include an `"items"` property to specify the type of items contained inside the collection. You may nest collections (`type` of `items` is also `"collection"`).
* The reference parameter is mandatory for annotations and classes, but optional for other types. Its effect differs depending on the input or output parameter used.
  * Inputs use references as constraint:
    * Each rectangle annotation of `my_rectangles` has to be an annotation of one of the input WSIs `my_wsis`.
    * Each float primitive of `my_thresholds` must reference one of the rectangle annotations in `my_rectangles`.
  * Outputs use references for semantic purposes:
    * Each integer value of the nested collection `tumor_cell_counts` will refer to one rectangle annotation of `my_rectangles`.
    * Each float value of the collection `avg_tumor_cell_counts` will refer to one of the inner collections of `my_rectangles`.

For more information about how references might be used, have a look at [Purpose of References](specs/references.md#).

## App API Usage

```python
import os
import requests
from PIL import Image
from io import BytesIO

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def count_tumor_cells(wsi_tile: Image, threshold: float):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return int(threshold * 100)

def compute_avg(tumor_cell_counts: dict):
    """
    pretends to do something useful

    Parameters:
        tumor_cell_counts: contains list of items with numeric value
    """
    values = [item["value"] for item in tumor_cell_counts["items"]]
    return sum(values) / len(values)

def get_processing_data(my_wsi: dict, my_rectangles: dict):
    """
    get wsi rectangle dict
    """
    data = {}
    for wsi in my_wsis["items"]:
        data[wsi["id"]] = get_rectangles_for_wsi(my_rectangles, wsi["id"])
    return data


def get_rectangles_for_wsi(my_rectangles: dict, wsi_id: str):
    """
    get rectangle collection with rectangles referencing given wsi_id
    """
    for rectangle_collection in my_rectangles["items"]:
        if rectangle_collection["reference_id"] == wsi_id:
            return rectangle_collection


def get_threshold_for_rectangle(my_thresholds: dict, rectangle_id: str):
    """
    get threshold for given rectangle_id
    """
    thresholds = {}
    for threshold_collection in my_thresholds["items"]:
        for threshold in threshold_collection["items"]:
            if threshold["reference_id"] == rectangle_id:
                return threshold["value"]
    return None

my_wsis = get_input("my_wsis")
my_rectangles = get_input("my_rectangles")
my_thresholds = get_input("my_thresholds")

tumor_cell_counts = {"item_type": "collection", "items": []}
avg_tumor_cell_counts = {"item_type": "float", "items": []}

data = get_processing_data(my_wsis, my_rectangles)

for wsi_id in data:
    rectangles = data[wsi_id]["items"]

    wsi_tumor_cell_counts = {"item_type": "integer", "items": []}

    for rectangle in rectangles:
        wsi_tile = get_wsi_tile(wsi_id, rectangle)

        tumor_cell_count = {
            "name": "cell count tumor",  # choose name freely
            "type": "integer",
            "value": count_tumor_cells(wsi_tile, get_threshold_for_rectangle(my_thresholds, rectangle["id"])),
            "reference_id": rectangle["id"],  # set reference to rectangle
            "reference_type": "annotation",
        }
        wsi_tumor_cell_counts["items"].append(tumor_cell_count)

    wsi_avg_tumor_cell_count = {
        "name": "avg tumor cell count",
        "type": "float",
        "value": compute_avg(wsi_tumor_cell_counts),
        "reference_id": data[wsi_id]["id"],  # set reference to rectangle collection
        "reference_type": "collection",
    }

    tumor_cell_counts["items"].append(wsi_tumor_cell_counts)
    avg_tumor_cell_counts["items"].append(wsi_avg_tumor_cell_count)

post_output("tumor_cell_counts", tumor_cell_counts)
post_output("avg_tumor_cell_counts", avg_tumor_cell_counts)

put_finalize()
```

## Further Examples

For more examples regarding input types and their declaration in the EAD see [Sample App Inputs](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/others/_12_eats_input_templates).
