# Configuration Settings

Although we strongly suggest to handle all computation inside your app, we understand that this may require a big amount of effort depending on your infrastructure, e.g. having a well established API where WSI tiles can be send to for processing. In order to do the integration of your app into the EMPAIA platform in small steps, you may want to first just develop an EMPAIA compliant app to serve as a proxy to your own API. As storing credentials to access this API inside the app is problematic in mutliple ways, e.g. not being able to change them if compromised, we offer the use of a global config store as part of the EMPAIA marketplace.

**Note:** If the app sends data to an external API, the property `data_transmission_to_external_service_provider` must be included and set to `true`.

In this section we will:
* Add global configuration settings.

## Configuration

* Global configurations:
  * private_api_username
  * private_api_password
  * optional_parameter

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Rectangle annotation (key: `my_rectangle`) specifying the region to be processed.

## Outputs

* 1 Tumor cell count (key: `tumor_cell_count`).

## EAD

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 08",
    "name_short": "TA08",
    "namespace": "org.empaia.vendor_name.tutorial_app_08.v1",
    "description": "Human readable description",
    "configuration": {
        "private_api_username": {
            "type": "string",
            "optional": false,
            "storage": "global"
        },
        "private_api_password": {
            "type": "string",
            "optional": false,
            "storage": "global"
        },
        "optional_parameter": {
            "type": "integer",
            "optional": true,
            "storage": "global"
        }
    },
    "data_transmission_to_external_service_provider": true,
    "inputs": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "inputs.my_wsi"
        }
    },
    "outputs": {
        "tumor_cell_count": {
            "type": "integer"
        }
    }
}
```

## App API Usage

```python
import os
from typing import List
import requests
from PIL import Image
from io import BytesIO

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def get_configuration():
    """
    gets all configuration parameters
    """
    url = f"{APP_API}/v0/{JOB_ID}/configuration"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def count_tumor_cells_external_api(wsi_tile: Image, user: str, password: str, parameter: int):
    """
    pretends to do request to an external api

    Parameters:
        wsi_tile: WSI image tile
        user: user for external api
        password: password for external api
        parameter: some parameter
    """
    return 42


my_wsi = get_input("my_wsi")
my_rectangle = get_input("my_rectangle")
wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

configuration = get_configuration()
user = configuration["private_api_username"]
password = configuration["private_api_password"]
parameter = configuration.get("optional_parameter", 84)

tumor_cell_count = {
    "name": "cell count tumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells_external_api(wsi_tile, user, password, parameter),
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
```
