# App Developer Documentation

Welcome to the EMPAIA App Developer Docs. The following guides provide you with the required information to create, describe and publish your data processing applications (apps) for histology, in a way that they are compatible with the EMPAIA Platform.

To gain a better understanding of the infrastrucutre provided by EMPAIA, please take a look at the documents linked under [References](#references). Afterwards we suggest to read through the [Getting Started](getting_started.md#) page.

App developers can check the API compliance of their apps using the [EMPAIA App Test Suite (EATS)](eats/eats.md#).

## Contact

* If: you have found an error in this documentation or would like to request an EAD feature please use the [Docs Issue Tracker](https://gitlab.com/empaia/documentation/app-developer-docs/-/issues)
* Elif: you have found a software bug or would like to request an EATS feature please use the [EATS Issue Tracker](https://gitlab.com/empaia/integration/empaia-app-test-suite/-/issues)
* Else: contact us directly via [dev-support@empaia.org](mailto:dev-support@empaia.org)

## References

### Publications

*  [EMPAIA App Interface: An open and vendor-neutral interface for AI applications in pathology](https://www.sciencedirect.com/science/article/pii/S0169260721006702)

### Open Source Repositories

* [App Developer Docs](https://gitlab.com/empaia/documentation/app-developer-docs/)
* [EMPAIA App Test Suite](https://gitlab.com/empaia/integration/empaia-app-test-suite/)
* [EMPAIA Platform Components](https://gitlab.com/empaia/)

### EMPAIA Platform Components

Detailed overview of the EMPAIA platform and its components.

<img src="images/detailed_apis.svg"></img>

### Presentation Slides

* <a href="resources/2022-06-01_App_Development.pdf" download>2022-06-01_App_Development.pdf</a>

## Change Log

### 2022-10-12

* EATS 2.0.23 released
* Updated EATS Services
* Fixed some minor issues
* Extended WBS v2 Scopes API
  * Added additional query filter for classes and primitives(see [Class Queries](specs/workbench_api.md#class-queries) or [Primitive Queries](specs/workbench_api.md#primitive-queries) for more details)
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2022-09-09

* EATS 2.0.21 released
* Updated EATS Services
* Fixed some minor issues
* Extended WBS v2 Scopes API
  * Added state of examination when retieving the scope by id (see [Scope Panel](specs/workbench_api.md#scope-panel) for more details)
  * Added additional query parameter when retrieving collections by id (see [Collection Queries](specs/workbench_api.md#collection-queries) for more details)
* Fixed a CSP issue when using a Safari browser
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2022-07-22

* EATS 2.0.13 released
* Updated EATS Services
* Fixed some minor issues
* Added obligation for Apps with a custom App UI to declare additionally needed Content Security Policies in an app-ui-config-file during app registration (see [App with App UI](eats/apps_with_ui.md#register-app) for more information)
* Added additional information regarding Content-Security-Policy in section [Web Security](specs/app_ui_requirements.md#web_security)
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2022-06-20

* added [best practices](tutorial_backend/tutorial.md#best-practices) section in the backend tutorial

### 2022-06-07

* EATS 2.0.9 released
* Updated EATS Services
* Fixed some minor issues
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2022-04-28

* EATS 2.0.5 released
* Updated error in WBS v2 Scopes OpenAPI Documentation
* Updated some EATS Services

### 2022-04-27

* The Vendor App Communication Interface (VACI) for App UIs can now be installed as npm package from [npmjs.com](https://www.npmjs.com/package/@empaia/vendor-app-communication-interface)

### 2022-04-25

* EATS 2.0.4 released
* Updated internal build process

### 2022-04-19

* EATS 2.0.3 released
* Updated some errors in EATS README.md

### 2022-04-08

* EATS 2.0.2 released
  * Beginning with EATS Version 2.0.2 all releases are only available on PyPI (Python Package Index)
  * Test releases are available on Test PyPI
* Most EATS Services and Frontends are no longer build locally but are pulled from the GitLab Docker Registry instead
* Updated EATS Services
  * Minor adjustments to API descriptions where necessary

### 2022-03-15

* EATS 2.0.1 released (<a href="resources/empaia_app_test_suite-2.0.1.zip">download</a>).
  * Support for additional Tutorial App 09
* Updated Tutorial: Backend
  * new Tutorial App [Additional Inputs](tutorial_backend/additional_inputs.md#)
* Updated [Tutorial: Frontend](tutorial_frontend/tutorial.md#)
* Updated [Specs Section](specs/specs.md#) with additional explanations
* Updated [Glossary](glossary.md#)

### 2022-03-07

* EATS 2.0.0 released (<a href="resources/empaia_app_test_suite-2.0.0.zip">download</a>).
  * Integration of Workbench Client 2.0
    * App developer can provide a custom App UI to setup jobs and visualize the results
  * Integration of Workbench Service 2.0
    * Generic API for App UIs to retrieve all app related data (e.g., job inputs, job results)
