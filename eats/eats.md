# EMPAIA App Test Suite (EATS)

The *EMPAIA App Test Suite (EATS)* provides CLI commands to help you in the development process of creating an app for the EMPAIA ecosystem.

With the EATS it is possible to check if

* the syntax of your app's EAD is correct
* the requests of your containerized app against the App API are correct

You can also start your app via the EMPAIA Workbench Client, process jobs or view the results of previously completed job runs, to get an idea of the user experience.

If your app uses an app specific App UI you can use the new Workbench Client 2.0 to test your App UI in terms of user experience, usage of the Workbench API and integration.
