# App without App UI

This section will guide you through using the EATS with one of the sample apps from the [Tutorial: Backend](tutorial_backend/tutorial.md#), i.e. _04_output_references_output.

Afterwards you will have executed every step necessary to test your own app and view the results.

## Prepare App

We need to build the docker image for the example app of the tutorial:

```bash
docker build -t app04 ./sample-apps/sample_apps/valid/tutorial/_04_output_references_output/
```

## Register App

Next we need to register our app at our locally running Marketplace-Service. The command to register the example app requires the following parameters:

- the docker image name: `app04`
- the path to the ead: `ead.json`

?> If your app requires configuration parameters, `--config-file <path/to/configuration.json>` must be appended.

```bash
cd ./sample-apps/sample_apps/valid/tutorial/_04_output_references_output/
eats apps register ead.json app04 > app.env
```

The command will create the file `app.env` which contains an `APP_ID`, which we can store as an environment variable for later use with:

```bash
export $(xargs <app.env)
echo $APP_ID
```

To view all registered apps use:

```bash
eats apps list
```

## Register and Run Job

Register a new job using the `APP_ID` and the folder containing the inputs and save the output to `job.env`. The output of the job register command includes the environment variables for your app (`EMPAIA_JOB_ID`, `EMPAIA_TOKEN`, `EMPAIA_APP_API`):

```bash
eats jobs register $APP_ID ./eats_inputs --v1 > ./job.env
```

!> Important change compared to previous EATS versions (1.2.3 and below): The new CLI parameter `--v1` is important to correctly create the input data in a user context and not a Scope context.

When a job  is registered, all inputs from `./eats_inputs` (except WSIs) are created and persisted in the Medical Data Service. Implicitly a case and an examination are created as well. When using the `--v1` flag, a static User ID is assigned as the creator of all posted inputs. The input data is then correctly displayed in the WBC 1.0. If the flag is omitted, a Scope ID obtained through the Workbench v2 API is assigned as the creator instead. The data is then correctly displayed in the WBC 2.0, but not the WBC 1.0. For more information see the [Workbench v1 Legacy Support](eats/workbench_v1_legacy_support.md#) section.

Run the app, providing the file including the environment variables `job.env`:

```bash
eats jobs run ./job.env
```

## Job Status

The job ID can be retrieved from the `job.env` file to be used in other commands.

```bash
export $(xargs <job.env)
echo $EMPAIA_JOB_ID
```

Regularly check the job's status until it is `COMPLETED`.

?> Depending on the elapsed time, the job will be in one of these states: `SCHEDULED`, `RUNNING` or `COMPLETED`. If the status is `ERROR`, the job did not complete successfully. See [Debugging](/eats/advanced.md#debugging) for help.

```bash
eats jobs status ${EMPAIA_JOB_ID}
```

## Results - JSON

To export the output primitives (as well as all other outputs):

```bash
eats jobs export ${EMPAIA_JOB_ID} ./job-outputs
```

## Results - Workbench Client

Open http://localhost:8888/wbc/ in a Browser to review the job results using the EMPAIA Workbench Client 1.0.

!> If you are using a different port for the **nginx reverse-proxy server**, the port in the URL must be adapted accordingly!

?> The following screenshots show the results of app **04_output_references_output**.

For simplicity, all slides are added to a static single case. Navigate to:

1) **Cases** and select the case:

<img src="images/app_test_suite/wbc_case(8888).png"></img>

2) **Examinations** and select the OPEN examination:

<img src="images/app_test_suite/wbc_examination(8888).png"></img>

3) **Apps** and select your app:

<img src="images/app_test_suite/wbc_app(8888).png"></img>

4) **Jobs** and select the latest job:

<img src="images/app_test_suite/wbc_job(8888).png"></img>

5a) Explore the tree view. E.g. click **\[Root\]** to see primitive outputs of your app with no reference or a reference to a collection (bottom right) (none in this job).

5b) Click the **\[WSI\]...** to view the slide and see primitive outputs referencing the slide (bottom right) (none in this job).

5c) Click **\[Rectangle\]...** to see primitive outputs referencing the item (bottom right) (none in this job). Also note, that depending on your position in the tree view, the annotations list on the left changes according to the references, i.e. (collections of) annotations referencing the **Rectangle**:

<img src="images/app_test_suite/wbc_job_results_collection(8888).png"></img>

5d) Click **\[Point\]...** to see primitive outputs referencing the item (bottom right):

<img src="images/app_test_suite/wbc_job_results_primitive(8888).png"></img>


?> The annotations **line** and **arrows** are not yet supported. Viewing a job containing one of these data types will result in no annotations loading.

?> IDs or whole objects in JSON-format can easily be copied to the clipboard in the Workbench Client. Therefore, the user needs to hold the key `i` (for ID) or `j` (for a string in JSON-notation) and click on a desired item in one of the UI-panels.
