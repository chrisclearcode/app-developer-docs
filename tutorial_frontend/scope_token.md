# Scope Token

The Workbench v2 Scopes API allows an App UI to access the functionalities provided by the EMPAIA platform. The Scopes API is protected and only accessible if a valid Scope Token is provided. The Scope Token itself is a JSON Web Token (JWT). The previous section App UI [initialization](tutorial_frontend/initialization.md#) describes how a token can be obtained during the App UI startup. This section describes how to use and renew the Scope Token.

Every API call to the Workbench v2 Scopes API (`/v2/scopes/`) requires an Authorization Header for verification. You have to use the Scope Token received by `addTokenListener()` with every API request. Keep in mind that the Scope Token expires after a certain amount of time. You can either choose to proactively renew the token before it expires (e.g. by decoding the JWT) or to renew after receiving an HTTP 401 error due to an expired token.

!> You should never assume or hardcode a certain expiration time, as this setting might be subject to change.

Pure JavaScript implementation can use XMLHttpRequest. With Angular it is advised to use HttpInterceptors to add the authorization header to every request (as demonstrated in the Sample App UI).


<!-- tabs:start -->
<!-- tab:JavaScript -->
```js
const xhr = new XMLHttpRequest();
xhr.setRequestHeader('Authorization', `Bearer ${token.value}`);
// the actual request
```
<!-- tab:TypeScript -->
```js
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authReq = request.clone({
      setHeaders: {
        ['Authorization']: `Bearer ${INSERT_TOKEN_HERE}`,
      }
    });

    return next.handle(authReq);
  }
}
```
<!-- tabs:end -->


If our token is expired, we need to request a new one via the `requestNewToken()` function of the Vendor App Communication Interface.

<!-- tabs:start -->
<!-- tab:JavaScript -->
```js
function mySampleFunction() {
  vendorAppCommunication.addTokenListener(function(token) {
    // all added event listeners will receive the new token,
    // every time after the requestNewToken() function was
    // called
  });
}

function requestToken() {
  vendorAppCommunication.requestNewToken();
}
```
<!-- tab:TypeScript -->
```ts
import {
  addTokenListener,
  requestNewToken,
} from '@empaia/vendor-app-communication-interface';

function mySampleFunction() {
  addTokenListener((token: Token) => {
    // all added event listeners will receive the new token,
    // every time after the requestNewToken() function was
    // called
  });
}

function requestToken() {
  requestNewToken();
}
```
<!-- tabs:end -->
