<!-- docs/_sidebar.md -->

* [Introduction]()
* [Getting started](getting_started)
* [Tutorial: Backend](tutorial_backend/tutorial)
* [Tutorial: Frontend](tutorial_frontend/tutorial)
  * [Sample App Backend](tutorial_frontend/sample_app_backend)
  * [App UI Initialization](tutorial_frontend/initialization)
  * [Scope Token](tutorial_frontend/scope_token)
  * [Workbench API Usage](tutorial_frontend/workbench_api_usage)
* [Specs](specs/specs)
* [EMPAIA App Test Suite (EATS)](eats/eats)
* [Publish](publish)
* [Glossary](glossary)
