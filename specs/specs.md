# Specs

If you have not yet done so, we suggest to first read through the tutorials to get familiar with the EMPAIA App Description (EAD), the App API and the Workbench API. The following sections provide detailed specifications for the topics presented during the tutorials.
