# EMPAIA App Description (EAD)

The source code and EAD for all scenarios mentioned in the tutorial sections can be downloaded from the [Sample Apps Repository (Tutorial: Backend)](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/tutorial) or [Sample Apps Repository (Tutorial: Frontend)](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/with_frontend) and used as template.

The full EAD JSON schema can be obtained from the [Definitions Repository](https://gitlab.com/empaia/integration/definitions/-/tree/main/ead).

## References

If you are curious on how references might be used, have a look at [Purpose of References](specs/references.md#).

The table below shows possible references between the different data types (read: "line has reference onto column"):

|                       | wsi | collection | primitive data types | annotation data types | class |
|-----------------------|-----|------------|----------------------|-----------------------|-------|
| wsi                   | -   | -          | -                    | -                     | -     |
| collection            | ×   | -          | -                    | ×                     | -     |
| primitive data types  | ×   | ×          | -                    | ×                     | -     |
| annotation data types | ×!  | -          | -                    | -                     | -     |
| class                 | -   | -          | -                    | ×!                    | -     |

?> The reference parameter is optional but enhances interpretabilty of your app and its results. Its effect differs depending if it is used within an input or output parameter:

* Inputs use references as constraint.
* If the reference parameter is left out, no reference-constraint will be used to validate input data.
* Outputs use reference for semantic purposes.
* If the reference parameter is left out, you will NOT be able to include a reference when sending your results. Therefore it will not be possible to determine which value refers to which rectangle annotation.

!> For annotation outputs, references are **NOT OPTIONAL** and **MUST** reference a WSI.

!> For class inputs and outputs, references are **NOT OPTIONAL** and **MUST** reference an annotation.

## Data Types

This section includes detailed information about all data types available and how to use them in the EAD.

### WSI Data Type

```JSON
{
    "my_wsi": {
        "type": "wsi",
        "description": "Human readable description",
    }
}
```

Required:

* `"type": "wsi"`

### Primitive Data Types

#### Integer

```JSON
{
    "my_int": {
        "type": "integer",
        "description": "Human readable description",
        "reference": "inputs.some_other_param" // or outputs.some_other_param
    }
}
```

Required:

* `"type": "integer"`

#### Float

```JSON
{
    "my_float": {
        "type": "float",
        "description": "Human readable description",
        "reference": "inputs.some_other_param"
    }
}
```

Required:

* `"type": "float"`

#### Bool

```JSON
{
    "my_bool": {
        "type": "bool",
        "description": "Human readable description",
        "reference": "inputs.some_other_param" // or outputs.some_other_param
    }
}
```

Required:

* `"type": "bool"`

#### String

```JSON
{
    "my_string": {
        "type": "string",
        "description": "Human readable description",
        "reference": "inputs.some_other_param" // or outputs.some_other_param
    }
}
```

Required:

* `"type": "string"`

### Annotation Data Types

For all annotation types:

```JSON
{
    "my_point": {
        "type": "Annotation type",
        "description": "Human readable description",
        "reference": "inputs.some_wsi_param",
        "classes": [
            "org.empaia.my_vendor.my_app.v1.classes.arbitrarily.nested.class",
            "org.empaia.global.v1.classes.roi",
        ]
    }
}
```

Required:
* `type` (one of `point`, `line`, `arrow`, `rectangle`, `polygon`, `circle`)
* `reference` pointing to a WSI or items of a WSI collection

Optional:

* `classes` defines a class constraint for the specified annotation. This stipulates that one (or more) of the given classes is assigned to the annotation.

### Collection Data Type

```JSON
{
    "my_collection": {
        "type": "collection",
        "description": "Human readable description",
        "reference": "inputs.some_other_param",
        "items": {
            "type": "any_type"
            // remaining properties depending on type
        }
    }
}
```

Required:

* `"type": "collection"`
* `items`: Inner object can be any data type, including `"collection"`

### Class Data Type

```JSON
{
    "namespace": "org.empaia.my_vendor.my_app.v1",
    "classes": {
        "tumor": {
            "pos": {
                "name": "Tumor positive",
                "description": "Human readable description"
            },
            "neg": {
                "name": "Tumor negative",
                "description": "Human readable description"
            }
        },
        "non_tumor": {
            "name": "Non tumor",
            "description": "Human readable description"
        }
    },
    "inputs": { // or "outputs"
        "my_class": {
            "type": "class",
            "description": "Human readable description",
            "reference": "inputs.some_annotation_param" // or outputs.some_annotation_param
        }
    }
}
```

Required:

* `"type": "class"`
* `reference` to an annotation or items of an annotation collection
* `classes` on top level must define all App specific classes. These can be arbitrarily nested. EMPAIA classes can always be used and must not be defined!

## Configuration

The `configuration` property is optional. It enables the definition of global parameters (key value pairs). Their values are the same for every running instance of the app independent of the execution environment.

?> If the app sends data to an external API, the property `data_transmission_to_external_service_provider` must be included and set to `true`. For further requirements refer to the [app requirements](specs/app_requirements.md#) section.

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "My app",
    "namespace": "org.empaia.my_vendor.my_app.v1",
    "description": "Human readable description",
    "configuration": {
        "private_api_username": {
            "type": "string",
            "optional": false,
            "storage": "global"
        },
        "private_api_password": {
            "type": "string",
            "optional": false,
            "storage": "global"
        }
    },
    "data_transmission_to_external_service_provider": true,
    "inputs": {
        // (...)
    },
    "outputs": {
        // (...)
    }
}
```

Required:
* `"type"` (one of `"boolean"`, `"integer"`, `"float"`, or `"string"`)
* `"storage": "global"`

## Other fields

The `"name"` and `"description"` fields in the EAD should contain english text. They are only used for documentation purposes and not exposed to the user.

The `name_short` field is displayed in the Workbench Client and limited to a length of 10 characters. Each app is represented with the vendor logo, the vendor name, the app's `name_short` and the app's version number. Therefore `name_short` must not contain the vendor name, while the `name` field should contain the vendor name for documentation purposes.
