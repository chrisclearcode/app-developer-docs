# App UI Requirements

## Web Security

The EMPAIA platform requires App UIs to comply with security Restrictions:

* do not load external fonts or icons via `<link>`
  * all resources must be part of the compiled / bundled App
* do not use other APIs except the Workbench v2 Scopes API at `/v2/scopes/`
* do not embed other web apps via iframe in your App UI
* do not access cookies, local storage or indexedDB
* do not access the DOM outside the iframe of your own App UI
* do not use lazy resource loading in your app

!> **Firefox users:** The current version of Firefox (102.0.1 at the time this documentation was published) uses the experimental "Storage Access API" and therefore will show a JavaScript error in the developer tools console starting with `document.requestStorageAccess()`. This error stems from the security settings used for App UIs and cannot be circumvented at the moment.

?> Please contact us, if certain restrictions are problematic for your App UI functionality or developement workflow.

The rules mentioned above are enforced through technical measures.

1. The Frontend Token is only valid for one minute, therefore all static resources must be loaded immediately
2. The iframe is limited to functionalities available through `sandbox="allow-scripts"`
3. The Content-Security-Policy headers enforced by the Workbench v2 Frontends API prevent the access to external APIs and resources

### Content-Security-Policy (CSP) <!-- {docsify-ignore} -->

The Workbench v2 Frontends API declares default CSP directives and policies as followed:

* `default-src http://localhost:8888`: Fallback for all derived CSP directives (e.g. `script-src`)
* `img-src http://localhost:8888 blob:`: Needed to show slide image data in a slide viewer UI component

?> Port 8888 is the default port of the NGINX reverse proxy. If this port is changed in the `eats services up` command via `--nginx-port <port>` the CSP settings are set accordingly.

The default CSP settings are set for all App UIs. If your App UI needs additional CSP directives, they must be declared in a separate `app-ui-config-file.json` file.

Currently suported CSP directives and policies are:

* `script-src`: possible policies are `unsafe-inline`, `unsafe-eval`
* `style-src`: possible policies are `unsafe-inline`, `unsafe-eval`
* `font-src`: possible policies are `unsafe-inline`, `unsafe-eval`

If any of these derectives are explicitly used, the source policy `http://localhost:<nginx-port>` is implicitly set.

An example of a valid `app-ui-config-file.json` file (requesting all available CSP settings) would be:

```Json
{
  "csp": {
    "script_src": {
      "unsafe_inline": true,
      "unsafe_eval": true
    },
    "style_src": {
      "unsafe_inline": true,
      "unsafe_eval": true
    },
    "font_src": {
      "unsafe_inline": true,
      "unsafe_eval": true
    }
  }
}
```

All directives and policy settings are optional and can be omitted if not needed (setting policies explicitly to `false` is also possible).

!> Please note that every additional CSP directive and policy has an impact on overall web security of an App UI. Therefore CSP settings should only be used if absolutely necessary (e.g. required by used framework). If additional CSP settings are required for an App UI, it must be ensured that suitable security measures are taken (either by the framework or manually).

A realistic and minimal example of an `app-ui-config-file.json` is shown in the EATS section [Apps with App UI](eats/apps_with_ui.md#register-app).

?> If additional CSP directives are required by a certain framework, please contact us. We will evaluate the possibility of including additional options in the CSP settings.
